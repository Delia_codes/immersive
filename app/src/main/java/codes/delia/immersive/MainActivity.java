package codes.delia.immersive;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;


public class MainActivity extends Activity implements CompoundButton.OnCheckedChangeListener, View.OnClickListener {

    private static final String EXTRA_FLAG_SYSTEM_UI_VISIBILITY = "extra_flag_system_ui_visibility";
    private CheckBox checkFullScreen;
    private CheckBox checkHideNavigation;
    private CheckBox checkImmersive;
    private CheckBox checkImmersiveSticky;
    private CheckBox checkLayoutFullScreen;
    private CheckBox checkLayoutHideNavigation;
    private CheckBox checkLowProfile;
    private CheckBox checkFlagVisible;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setTitle(R.string.delia_codes);

        checkFullScreen = (CheckBox) findViewById(R.id.system_ui_flag_fullscreen);
        checkFullScreen.setOnCheckedChangeListener(this);

        checkHideNavigation = (CheckBox) findViewById(R.id.system_ui_flag_hide_navigation);
        checkHideNavigation.setOnCheckedChangeListener(this);

        checkImmersive = (CheckBox) findViewById(R.id.system_ui_flag_immersive);
        checkImmersive.setOnCheckedChangeListener(this);

        checkImmersiveSticky = (CheckBox) findViewById(R.id.system_ui_flag_immersive_sticky);
        checkImmersiveSticky.setOnCheckedChangeListener(this);

        checkLayoutFullScreen = (CheckBox) findViewById(R.id.system_ui_flag_layout_fullscreen);
        checkLayoutFullScreen.setOnCheckedChangeListener(this);

        checkLayoutHideNavigation = (CheckBox) findViewById(R.id.system_ui_flag_layout_hide_navigation);
        checkLayoutHideNavigation.setOnCheckedChangeListener(this);

        checkLowProfile = (CheckBox) findViewById(R.id.system_ui_flag_low_profile);
        checkLowProfile.setOnCheckedChangeListener(this);

        checkFlagVisible = (CheckBox) findViewById(R.id.system_ui_flag_visible);
        checkFlagVisible.setOnCheckedChangeListener(this);

        findViewById(R.id.btn_open_activity).setOnClickListener(this);

        int systemUiVisibility = getWindow().getDecorView().getSystemUiVisibility();
        if(getIntent() != null && getIntent().hasExtra(EXTRA_FLAG_SYSTEM_UI_VISIBILITY)) {
            systemUiVisibility = getIntent().getIntExtra(EXTRA_FLAG_SYSTEM_UI_VISIBILITY,systemUiVisibility);
        }
        getWindow().getDecorView().setSystemUiVisibility(systemUiVisibility);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        int id = buttonView.getId();
        switch(id) {
            case R.id.system_ui_flag_fullscreen:
            break;
            case R.id.system_ui_flag_hide_navigation:
            break;
            case R.id.system_ui_flag_immersive:
                checkLayoutHideNavigation.setChecked(true);
            break;
            case R.id.system_ui_flag_immersive_sticky:
                if(checkFullScreen.isChecked() || checkLayoutHideNavigation.isChecked()) {
                    checkLayoutHideNavigation.setChecked(true);
                }
            break;
            case R.id.system_ui_flag_layout_fullscreen:
            break;
            case R.id.system_ui_flag_layout_hide_navigation:
            break;
            case R.id.system_ui_flag_low_profile:
            break;
            case R.id.system_ui_flag_visible:
            break;
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if(id == R.id.btn_open_activity) {
            startActivityWithFlags();
        }
    }

    private void startActivityWithFlags() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(EXTRA_FLAG_SYSTEM_UI_VISIBILITY, 0
                | (checkFullScreen.isChecked() ? View.SYSTEM_UI_FLAG_FULLSCREEN : 0)
                | (checkHideNavigation.isChecked() ? View.SYSTEM_UI_FLAG_HIDE_NAVIGATION : 0)
                | (checkImmersive.isChecked() ? View.SYSTEM_UI_FLAG_IMMERSIVE : 0)
                | (checkImmersiveSticky.isChecked() ? View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY : 0)
                | (checkLayoutFullScreen.isChecked() ? View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN : 0)
                | (checkLayoutHideNavigation.isChecked() ? View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION : 0)
                | (checkLowProfile.isChecked() ? View.SYSTEM_UI_FLAG_LOW_PROFILE : 0)
                | (checkFlagVisible.isChecked() ? View.SYSTEM_UI_FLAG_VISIBLE : 0)
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
        );
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        startActivity(intent);

    }
}
